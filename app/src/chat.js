import React from 'react';
import './chat.css';

function GetTime(){
    var date, TimeType, hour, minutes, seconds, fullTime;
     
    // Creating Date() function object.
    date = new Date();

    // Getting current hour from Date object.
    hour = date.getHours(); 

    // Checking if the Hour is less than equals to 11 then Set the Time format as AM.
    if(hour <= 11){
      TimeType = 'AM';
    }
    else{
      // If the Hour is Not less than equals to 11 then Set the Time format as PM.
      TimeType = 'PM';
    }

    // IF current hour is grater than 12 then minus 12 from current hour to make it in 12 Hours Format.
    if( hour > 12 ){
      hour = hour - 12;
    }

    // If hour value is 0 then by default set its value to 12, because 24 means 0 in 24 hours time format. 
    if( hour === 0 ){
        hour = 12;
    } 

    // Getting the current minutes from date object.
    minutes = date.getMinutes();

    // Checking if the minutes value is less then 10 then add 0 before minutes.
    if(minutes < 10){
      minutes = '0' + minutes.toString();
    }

    //Getting current seconds from date object.
    seconds = date.getSeconds();
 
    // If seconds value is less than 10 then add 0 before seconds.
    if(seconds < 10)
    {
      seconds = '0' + seconds.toString();
    }

    // Adding all the variables in fullTime variable.
    fullTime = hour.toString() + ':' + minutes.toString() + ':' + seconds.toString() + ' ' + TimeType.toString();
 
    return fullTime;
}

class ChatMessage extends React.Component {
    static defaultProps = {
        fromMe: false,
    };

    constructor(){
        super();
        this.state ={
            update : false,
        }
    }

    shouldComponentUpdate(nextProps,nextState){
       if(this.state.update === nextState.update){
          return false;
       } else {
          return true;
       }
    }

    render() {
        let className = 'chat__message'
        if (this.props.fromMe) {
            className += ' chat__message--from-me';
        } else {
            className += ' chat__message--from-her';
        }

        return <div className={className}>
            {this.props.children}
            <div className='chat__message--time'>
                {GetTime()}
            </div>
        </div>;
    }
}

class Chat extends React.Component {
    static defaultProps = {
        onSend: () => {},
        messages: [],
    };

    state = {
        userInput: '',
    };

    handleTextareaChange = (e) => {
        this.setState({userInput: e.target.value});
    }

    handleSendClick = (e) => {
        var input_ = this.state.userInput;
        
        if(input_.replace(/\s/g,'').length){
            this.props.onSend(this.state.userInput);
            this.setState({userInput: ''});  
        }               
    }

    keydownHandler = (e) => {
        if(e.keyCode===13 && e.ctrlKey){
            this.handleSendClick();
        }
    }

    componentDidMount(){
        document.addEventListener('keydown',this.keydownHandler);
    }

    componentWillUnmount(){
        document.removeEventListener('keydown',this.keydownHandler);
    }

    render() {
        var input_ = this.state.userInput;
        var empty;

        if(input_.replace(/\s/g,'').length){
            empty = false;
        }else empty = true;

        return <div className='chat'>
            <div className='chat__log'>{this.props.messages}</div>
            <div className='chat__input'>
                <textarea
                    rows={4}
                    value={this.state.userInput}
                    onChange={this.handleTextareaChange}
                />
                <button className='send__button'
                    onClick={this.handleSendClick}
                    disabled={empty}
                ></button>
            </div>
        </div>;
    }
}


export default Chat;

export { ChatMessage };