const raw = `
"Irrationality is the square root of all evil"
		-- Douglas Hofstadter
SEPARATOR
We all know Linux is great... it does infinite loops in 5 seconds.
	- Linus Torvalds about the superiority of Linux on the Amsterdam Linux
    Symposium
SEPARATOR
When properly administered, vacations do not diminish productivity: for
every week you're away and get nothing done, there's another when your boss
is away and you get twice as much done.
		-- Daniel B. Luten
SEPARATOR
"Buy land.  They've stopped making it."
-- Mark Twain
SEPARATOR
You can not get anything worthwhile done without raising a sweat.
		-- The First Law Of Thermodynamics

What ever you want is going to cost a little more than it is worth.
		-- The Second Law Of Thermodynamics

You can not win the game, and you are not allowed to stop playing.
		-- The Third Law Of Thermodynamics
SEPARATOR
Nothing is finished until the paperwork is done.
SEPARATOR
If you are going to run a rinky-dink distro made by a couple of
volunteers, why not run a rinky-dink distro made by a lot of volunteers?
		-- Jaldhar H. Vyas on debian-devel
SEPARATOR
Kent's Heuristic:
	Look for it first where you'd most like to find it.
SEPARATOR
The only reward of virtue is virtue.
		-- Ralph Waldo Emerson
SEPARATOR
The lights are on,
but you're not home;
Your will
is not your own;
Your heart sweats,
Your teeth grind;
Another kiss
and you'll be mine...

You like to think that you're immune to the stuff
(Oh Yeah!)
It's closer to the truth to say you can't get enough;
You know you're gonna have to face it,
You're addicted to love!"
		-- Robert Palmer
SEPARATOR
snappy repartee:
	What you'd say if you had another chance.
SEPARATOR
I have discovered the art of deceiving diplomats. I tell them the truth
and they never believe me.
		-- Camillo Di Cavour
SEPARATOR
6 oz. orange juice
1 oz. vodka
1/2 oz. Galliano
		Harvey Wallbangers
SEPARATOR
<doogie> there is one bad thing about having a cell phone.
<doogie> I can be reached at any time. :|
<wmono> that's why I leave mine off at all times. ;>
SEPARATOR
If a person (a) is poorly, (b) receives treatment intended to make him better,
and (c) gets better, then no power of reasoning known to medical science can
convince him that it may not have been the treatment that restored his health.
		-- Sir Peter Medawar, "The Art of the Soluble"
SEPARATOR
Programmers used to batch environments may find it hard to live without
giant listings; we would find it hard to use them.
		-- D.M. Ritchie
SEPARATOR
if (instr(buf,sys_errlist[errno]))  /* you don't see this */
             -- Larry Wall in eval.c from the perl source code
SEPARATOR
All I kin say is when you finds yo'self wanderin' in a peach orchard,
ya don't go lookin' for rutabagas.
		-- Kingfish
SEPARATOR
	A grade school teacher was asking students what their parents did
for a living.  "Tim, you be first," she said.  "What does your mother do
all day?"
	Tim stood up and proudly said, "She's a doctor."
	"That's wonderful.  How about you, Amie?"
	Amie shyly stood up, scuffed her feet and said, "My father is a
mailman."
	"Thank you, Amie," said the teacher.  "What about your father, Billy?"
	Billy proudly stood up and announced, "My daddy plays piano in a
whorehouse."
	The teacher was aghast and promptly changed the subject to geography.
Later that day she went to Billy's house and rang the bell.  Billy's father
answered the door.  The teacher explained what his son had said and demanded
an explanation.
	Billy's father replied, "Well, I'm really an attorney.  But how do
you explain a thing like that to a seven-year-old child?"
SEPARATOR
"Good afternoon, madam.  How may I help you?"

"Good afternoon.  I'd like a FrintArms HandCannon, please."

"A--?  Oh, now, that's an awfully big gun for such a lovely lady.  I
mean, not everybody thinks ladies should carry guns at all, though I
say they have a right to.  But I think... I might... Let's have a look
down here.  I might have just the thing for you.  Yes, here we are!
Look at that, isn't it neat?  Now that is a FrintArms product as well,
but it's what's called a laser -- a light-pistol some people call
them.  Very small, as you see; fits easily into a pocket or bag; won't
spoil the line of a jacket; and you won't feel you're lugging half a
tonne of iron around with you.  We do a range of matching accessories,
including -- if I may say so -- a rather saucy garter holster.  Wish I
got to do the fitting for that!  Ha -- just my little joke.  And
there's *even*... here we are -- this special presentation pack: gun,
charged battery, charging unit, beautiful glider-hide shoulder holster
with adjustable fitting and contrast stitching, and a discount on your
next battery.  Full instructions, of course, and a voucher for free
lessons at your local gun club or range.  Or there's the *special*
presentation pack; it has all the other one's got but with *two*
charged batteries and a night-sight, too.  Here, feel that -- don't
worry, it's a dummy battery -- isn't it neat?  Feel how light it is?
Smooth, see?  No bits to stick out and catch on your clothes, *and*
beautifully balanced.  And of course the beauty of a laser is, there's
no recoil.  Because it's shooting light, you see?  Beautiful gun,
beautiful gun; my wife has one.  Really.  That's not a line, she
really has.  Now, I can do you that one -- with a battery and a free
charge -- for ninety-five; or the presentation pack on a special
offer for one-nineteen; or this, the special presentation pack, for
one-forty-nine."

"I'll take the special."

"Sound choice, madam, *sound* choice.  Now, do--?"

"And a HandCannon, with the eighty-mill silencer, five GP clips, three
six-five AP/wire-fl'echettes clips, two bipropellant HE clips, and a
Special Projectile Pack if you have one -- the one with the embedding
rounds, not the signalers.  I assume the night-sight on this toy is
compatible?"

"Aah... yes,  And how does madam wish to pay?"

She slapped her credit card on the counter.  "Eventually."

	  -- Iain M. Banks, "Against a Dark Background"
SEPARATOR
A visit to a fresh place will bring strange work.
SEPARATOR
If opportunity came disguised as temptation, one knock would be enough.
SEPARATOR
I'm having a BIG BANG THEORY!!
SEPARATOR
If you would know the value of money, go try to borrow some.
		-- Ben Franklin
SEPARATOR
<Crow-> these stupid head hunters want resumes in ms word format
<Crow-> can you write shit in tex and convert it to word?
<Overfiend> \\converttoword{shit}
SEPARATOR
Any time things appear to be going better, you have overlooked something.
SEPARATOR
I have had my television aerials removed.  It's the moral equivalent
of a prostate operation.
		-- Malcolm Muggeridge
SEPARATOR
Anyone who uses the phrase "easy as taking candy from a baby" has never
tried taking candy from a baby.
		-- Robin Hood
SEPARATOR
I would much rather have men ask why I have no statue, than why I have one.
		-- Marcus Procius Cato
SEPARATOR
"You need tender loving care once a week - so that I can slap you into shape."
- Ellyn Mustard
SEPARATOR
You have a massage (from the Swedish prime minister).
SEPARATOR
Pollyanna's Educational Constant:
	The hyperactive child is never absent.
SEPARATOR
My NOSE is NUMB!
SEPARATOR
What is research but a blind date with knowledge?
		-- Will Harvey
SEPARATOR
I steal.
		-- Sam Giancana, explaining his livelihood to his draft board

Easy.  I own Chicago.  I own Miami.  I own Las Vegas.
		-- Sam Giancana, when asked what he did for a living
SEPARATOR
Hindsight is an exact science.
SEPARATOR
Home life as we understand it is no more natural to us than a cage is
to a cockatoo.
		-- George Bernard Shaw
SEPARATOR
When I works, I works hard.
When I sits, I sits easy.
And when I thinks, I goes to sleep.
SEPARATOR
A pretty foot is one of the greatest gifts of nature... please send me your
last pair of shoes, already worn out in dancing... so I can have something
of yours to press against my heart.
		-- Goethe
SEPARATOR
"I'm a doctor, not a mechanic."
		-- "The Doomsday Machine", when asked if he had heard of
		   the idea of a doomsday machine.
"I'm a doctor, not an escalator."
		-- "Friday's Child", when asked to help the very pregnant
		   Ellen up a steep incline.
"I'm a doctor, not a bricklayer."
		-- Devil in the Dark", when asked to patch up the Horta.
"I'm a doctor, not an engineer."
		-- "Mirror, Mirror", when asked by Scotty for help in
		   Engineering aboard the ISS Enterprise.
"I'm a doctor, not a coalminer."
		-- "The Empath", on being beneath the surface of Minara 2.
"I'm a surgeon, not a psychiatrist."
		-- "City on the Edge of Forever", on Edith Keeler's remark
		   that Kirk talked strangely.
"I'm no magician, Spock, just an old country doctor."
		-- "The Deadly Years", to Spock while trying to cure the
		   aging effects of the rogue comet near Gamma Hydra 4.
"What am I, a doctor or a moonshuttle conductor?"
		-- "The Corbomite Maneuver", when Kirk rushed off from a
		   physical exam to answer the alert.
SEPARATOR
We were young and our happiness dazzled us with its strength.  But there was
also a terrible betrayal that lay within me like a Merle Haggard song at a
French restaurant. [...]
	I could not tell the girl about the woman of the tollway, of her milk
white BMW and her Jordache smile.  There had been a fight.  I had punched her
boyfriend, who fought the mechanical bulls.  Everyone told him, "You ride the
bull, senor.  You do not fight it."  But he was lean and tough like a bad
rib-eye and he fought the bull.  And then he fought me.  And when we finished
there were no winners, just men doing what men must do. [...]
	"Stop the car," the girl said.
	There was a look of terrible sadness in her eyes.  She knew about the
woman of the tollway.  I knew not how.  I started to speak, but she raised an
arm and spoke with a quiet and peace I will never forget.
	"I do not ask for whom's the tollway belle," she said, "the tollway
belle's for thee."
	The next morning our youth was a memory, and our happiness was a lie.
Life is like a bad margarita with good tequila, I thought as I poured whiskey
onto my granola and faced a new day.
		-- Peter Applebome, International Imitation Hemingway
		   Competition
SEPARATOR
The coast was clear.
		-- Lope de Vega
SEPARATOR
Did you know ...

That no-one ever reads these things?
SEPARATOR
On my planet, to rest is to rest -- to cease using energy.  To me, it
is quite illogical to run up and down on green grass, using energy,
instead of saving it.
		-- Spock, "Shore Leave", stardate 3025.2
SEPARATOR
I did it just to piss you off.  :-P
        -- Branden Robinson in a message to debian-devel
SEPARATOR
There be sober men a'plenty, and drunkards barely twenty; there are men
of over ninety who have never yet kissed a girl.  But give me the rambling
rover, from Orkney down to Dover, we will roam the whole world over, and
together we'll face the world.
		-- Andy Stewart, "After the Hush"
SEPARATOR
I was appalled by this story of the destruction of a member of a valued
endangered species.  It's all very well to celebrate the practicality of
pigs by ennobling the porcine sibling who constructed his home out of
bricks and mortar.  But to wantonly destroy a wolf, even one with an
excessive taste for porkers, is unconscionable in these ecologically
critical times when both man and his domestic beasts continue to maraud
the earth.
		Sylvia Kamerman, "Book Reviewing"
SEPARATOR
A LISP programmer knows the value of everything, but the cost of nothing.
		-- Alan Perlis
SEPARATOR
Imagination is more important than knowledge.
		-- Albert Einstein
SEPARATOR
Yes, but which self do you want to be?
SEPARATOR
Reading is thinking with someone else's head instead of one's own.
SEPARATOR
Practical people would be more practical if they would take a little
more time for dreaming.
		-- J. P. McEvoy
SEPARATOR
I don't want a pickle,
	I just wanna ride on my motorsickle.
And I don't want to die,
	I just want to ride on my motorcy.
Cle.
		-- Arlo Guthrie
SEPARATOR
It's better to burn out than it is to rust.
SEPARATOR
Never play pool with anyone named "Fats".
SEPARATOR
One day the King decided that he would force all his subjects to tell the
truth.  A gallows was erected in front of the city gates.  A herald announced,
"Whoever would enter the city must first answer the truth to a question
which will be put to him."  Nasrudin was first in line.  The captain of the
guard asked him, "Where are you going?  Tell the truth -- the alternative
is death by hanging."
	"I am going," said Nasrudin, "to be hanged on that gallows."
	"I don't believe you."
	"Very well, if I have told a lie, then hang me!"
	"But that would make it the truth!"
	"Exactly," said Nasrudin, "your truth."
SEPARATOR
Stellar rays prove fibbing never pays.  Embezzlement is another matter.
SEPARATOR
Your good nature will bring unbounded happiness.
SEPARATOR
It's not whether you win or lose, it's how you look playing the game.
SEPARATOR
Dinosaurs aren't extinct.  They've just learned to hide in the trees.
SEPARATOR
The good die young -- because they see it's no use living if you've got
to be good.
		-- John Barrymore
SEPARATOR
Better to light one candle than to curse the darkness.
		-- motto of the Christopher Society
SEPARATOR
Most people have two reasons for doing anything -- a good reason, and
the real reason.
SEPARATOR
default, n.:
	[Possibly from Black English "De fault wid dis system is you,
	mon."] The vain attempt to avoid errors by inactivity.  "Nothing will
	come of nothing: speak again." -- King Lear.
		-- Stan Kelly-Bootle, "The Devil's DP Dictionary"
SEPARATOR
The heroic hours of life do not announce their presence by drum and trumpet,
challenging us to be true to ourselves by appeals to the martial spirit that
keeps the blood at heat.  Some little, unassuming, unobtrusive choice presents
itself before us slyly and craftily, glib and insinuating, in the modest garb
of innocence.  To yield to its blandishments is so easy.  The wrong, it seems,
is venial...  Then it is that you will be summoned to show the courage of
adventurous youth.
		-- Benjamin Cardozo
SEPARATOR
Deja vu:
	French., already seen; unoriginal; trite.
	Psychol., The illusion of having previously experienced
	something actually being encountered for the first time.
	Psychol., The illusion of having previously experienced
	something actually being encountered for the first time.
SEPARATOR
* SynrG notes that the number of configuration questions to answer in
  sendmail is NON-TRIVIAL
SEPARATOR
The Commandments of the EE:

 (9)	Trifle thee not with radioactive tubes and substances lest thou
	commence to glow in the dark like a lightning bug, and thy wife be
	frustrated and have not further use for thee except for thy wages.
(10)	Commit thou to memory all the words of the prophets which are
	written down in thy Bible which is the National Electrical Code,
	and giveth out with the straight dope and consoleth thee when
	thou hast suffered a ream job by the chief electrician.
(11)	When thou muckest about with a device in an unthinking and/or
	unknowing manner, thou shalt keep one hand in thy pocket.  Better
	that thou shouldest keep both hands in thy pockets than
	experimentally determine the electrical potential of an
	innocent-seeming device.
SEPARATOR
CF&C stole it, fair and square.
		-- Tim Hahn
SEPARATOR
It is up to us to produce better-quality movies.
	-- Lloyd Kaufman, producer of "Stuff Stephanie in the Incinerator"
SEPARATOR
The energy produced by the breaking down of the atom is a very poor kind
of thing.  Anyone who expects a source of power from the transformation
of these atoms is talking moonshine.
		-- Ernest Rutherford, after he had split the atom for
		   the first time
SEPARATOR
      ...and before I knew what I was doing, I had kicked the
      typewriter and threw it around the room and made it beg for
      mercy.  At this point the typewriter pleaded for me to dress
      him in feminine attire but instead I pressed his margin release
      over and over again until the typewriter lost consciousness.
      Presently, I regained consciousness and realized with shame what
      I had done.  My shame is gone and now I am looking for a
      submissive typewriter, any color, or model.  No electric
      typewriters please!
                        --Rick Kleiner
SEPARATOR
In case of fire, stand in the hall and shout "Fire!"
		-- The Kidner Report
SEPARATOR
* Endy needs to consult coffee :P
<Endy> coffee the bot person, not coffee the beverage :)
<knghtbrd> consulting the beverage may help too  =>
SEPARATOR
FORTUNE DISCUSSES THE OBSCURE FILMS: #5

THE ATOMIC GRANDMOTHER:
	This humorous but heart-warming story tells of an elderly woman
	forced to work at a nuclear power plant in order to help the family
	make ends meet.  At night, granny sits on the porch, tells tales
	of her colorful past, and the family uses her to cook barbecues
	and to power small electrical appliances.  Maureen Stapleton gives
	a glowing performance.
SEPARATOR
University politics are vicious precisely because the stakes are so small.
		-- C. P. Snow
SEPARATOR
Humor in the Court:
Q: Are you qualified to give a urine sample?
A: Yes, I have been since early childhood.
SEPARATOR
A woman of generous character will sacrifice her life a thousand times
over for her lover, but will break with him for ever over a question of
pride -- for the opening or the shutting of a door.
		-- Stendhal
SEPARATOR
Carson's Observation on Footwear:
	If the shoe fits, buy the other one too.
SEPARATOR
Here comes the orator, with his flood of words and his drop of reason.
SEPARATOR
I don't wanna argue, and I don't wanna fight,
But there will definitely be a party tonight...
SEPARATOR
He who has the courage to laugh is almost as much a master of the world
as he who is ready to die.
		-- Giacomo Leopardi
SEPARATOR
The secret of healthy hitchhiking is to eat junk food.
SEPARATOR
prairies, n.:
	Vast plains covered by treeless forests.
SEPARATOR
"Everybody is talking about the weather but nobody does anything about it."
-- Mark Twain
SEPARATOR
<muggles> i'm trying to convince some netcom admins i know to convert
          to Debian from RH, netgod, but they are DAMN stubborn
<muggles> why RH users so damned hard headed?
<Espy> it's the hat
SEPARATOR
"Being against torture ought to be sort of a multipartisan thing."
-- Karl Lehenbauer, as amended by Jeff Daiell, a Libertarian
SEPARATOR
There is nothing wrong with Southern California that a rise in the
ocean level wouldn't cure.
		-- Ross MacDonald
SEPARATOR
Down that path lies madness.  On the other hand, the road to hell is
paved with melting snowballs.
             -- Larry Wall in <1992Jul2.222039.26476@netlabs.com>
SEPARATOR
After years of research, scientists recently reported that there is,
indeed, arroz in Spanish Harlem.
SEPARATOR
Save gas, don't use the shell.
SEPARATOR
Kiss me, Kate, we will be married o' Sunday.
		-- William Shakespeare, "The Taming of the Shrew"
SEPARATOR
Voiceless it cries,
Wingless flutters,
Toothless bites,
Mouthless mutters.
SEPARATOR
When you have eliminated the impossible, whatever remains, however improbable,
must be the truth.
		-- Sherlock Holmes, "The Sign of Four"
SEPARATOR
The mother of the year should be a sterilized woman with two adopted children.
		-- Paul Ehrlich
SEPARATOR
Marriage is a romance in which the hero dies in the first chapter.
SEPARATOR
People who claim they don't let little things bother them have never
slept in a room with a single mosquito.
SEPARATOR
"Spare no expense to save money on this one."
		-- Samuel Goldwyn
SEPARATOR
TV is chewing gum for the eyes.
		-- Frank Lloyd Wright
SEPARATOR
modem, adj.:
	Up-to-date, new-fangled, as in "Thoroughly Modem Millie."  An
	unfortunate byproduct of kerning.

	[That's sic!]
SEPARATOR
The Moving Finger writes; and, having writ,
	Moves on: nor all they Piety nor Wit
Shall lure it back to cancel half a Line,
	Nor all thy Tears wash out a Word of it.
SEPARATOR
Hate is like acid.  It can damage the vessel in which it is stored as well
as destroy the object on which it is poured.
SEPARATOR
Canonical, adj.:
	The usual or standard state or manner of something.  A true story:
One Bob Sjoberg, new at the MIT AI Lab, expressed some annoyance at the use
of jargon.  Over his loud objections, we made a point of using jargon as
much as possible in his presence, and eventually it began to sink in.
Finally, in one conversation, he used the word "canonical" in jargon-like
fashion without thinking.
	Steele: "Aha!  We've finally got you talking jargon too!"
	Stallman: "What did he say?"
	Steele: "He just used \`canonical' in the canonical way."
SEPARATOR
Be circumspect in your liaisons with women.  It is better to be seen at
the opera with a man than at mass with a woman.
		-- De Maintenon
SEPARATOR
Over the years, I've developed my sense of deja vu so acutely that now
I can remember things that *have* happened before ...
SEPARATOR
Reach into the thoughts of friends,
And find they do not know your name.
Squeeze the teddy bear too tight,
And watch the feathers burst the seams.
Touch the stained glass with your cheek,
And feel its chill upon your blood.
Hold a candle to the night,
And see the darkness bend the flame.
Tear the mask of peace from God,
And hear the roar of souls in hell.
Pluck a rose in name of love,
And watch the petals curl and wilt.
Lean upon the western wind,
And know you are alone.
		-- Dru Mims
SEPARATOR
: But for some things, Perl just isn't the optimal choice.

(yet)   :-)
             -- Larry Wall in <199702221943.LAA20388@wall.org>
SEPARATOR
If you push the "extra ice" button on the soft drink vending machine, you won't
get any ice.  If you push the "no ice" button, you'll get ice, but no cup.
SEPARATOR
Telephone, n.:
	An invention of the devil which abrogates some of the advantages
	of making a disagreeable person keep his distance.
		-- Ambrose Bierce
SEPARATOR
You are standing on my toes.
SEPARATOR
The most important early product on the way to developing a good product
is an imperfect version.
SEPARATOR
I shot an arrow in to the air, and it stuck.
		-- graffito in Los Angeles

On a clear day,
U.C.L.A.
		-- graffito in San Francisco

There's so much pollution in the air now that if it weren't for our
lungs there'd be no place to put it all.
		-- Robert Orben
SEPARATOR
The evolution of the human race will not be accomplished in the ten thousand
years of tame animals, but in the million years of wild animals, because man
is and will always be a wild animal.
-- Charles Galton Darwin
SEPARATOR
My polyvinyl cowboy wallet was made in Hong Kong by Montgomery Clift!
SEPARATOR
Hickory Dickory Dock,
The mice ran up the clock,
The clock struck one,
The others escaped with minor injuries.
SEPARATOR
In Riemann, Hilbert or in Banach space
Let superscripts and subscripts go their ways.
Our asymptotes no longer out of phase,
We shall encounter, counting, face to face.
		-- Stanislaw Lem, "Cyberiad"
SEPARATOR
If you laid all of our laws end to end, there would be no end.
		-- Mark Twain
SEPARATOR
But that looks a little too much like a declaration for my tastes, when
in fact it isn't one.  So forget I mentioned it.
             -- Larry Wall in <199710011704.KAA21395@wall.org>
SEPARATOR
You will not censor me through bug terrorism.
	-- James Troup
SEPARATOR
Please, Mother!  I'd rather do it myself!
SEPARATOR
You possess a mind not merely twisted, but actually sprained.
SEPARATOR
The aim of science is to seek the simplest explanations of complex
facts.  Seek simplicity and distrust it.
		-- Whitehead.
SEPARATOR
Reality always seems harsher in the early morning.
SEPARATOR
The graveyards are full of indispensable men.
		-- Charles de Gaulle
SEPARATOR
Envy is a pain of mind that successful men cause their neighbors.
		-- Onasander
SEPARATOR
If they can make penicillin out of moldy bread, they can sure make
something out of you.
		-- Muhammad Ali
SEPARATOR
One measure of friendship consists not in the number of things friends
can discuss, but in the number of things they need no longer mention.
		-- Clifton Fadiman
SEPARATOR
Good teaching is one-fourth preparation and three-fourths good theatre.
		-- Gail Godwin
SEPARATOR
"There is no Father Christmas.  It's just a marketing ploy to make low income
parents' lives a misery."
"... I want you to picture the trusting face of a child, streaked with tears
because of what you just said."
"I want you to picture the face of its mother, because one week's dole won't
pay for one Master of the Universe Battlecruiser!"
		-- Filthy Rich and Catflap
SEPARATOR
As long as there are entrenched social and political distinctions
between sexes, races or classes, there will be forms of science whose
main function is to rationalize and legitimize these distinctions.
		-- Elizabeth Fee
SEPARATOR
The bland leadeth the bland and they both shall fall into the kitsch.
SEPARATOR
Work is of two kinds: first, altering the position of matter at or near
the earth's surface relative to other matter; second, telling other people
to do so.
		-- Bertrand Russell
SEPARATOR
Support Mental Health.  Or I'll kill you.
SEPARATOR
A man who turns green has eschewed protein.
SEPARATOR
If God had intended Man to program, we'd be born with serial I/O ports.
SEPARATOR
miracle:  an extremely outstanding or unusual event, thing, or accomplishment.
-- Webster's Dictionary
SEPARATOR
"He don't know me vewy well, DO he?"   -- Bugs Bunny
SEPARATOR
Here is an Appalachian version of management's answer to those who are
concerned with the fate of the project:
"Don't worry about the mule.  Just load the wagon."
-- Mike Dennison's hillbilly uncle
SEPARATOR
	There was a college student trying to earn some pocket money by
going from house to house offering to do odd jobs.  He explained this to
a man who answered one door.
	"How much will you charge to paint my porch?" asked the man.
	"Forty dollars."
	"Fine" said the man, and gave the student the paint and brushes.
	Three hours later the paint-splattered lad knocked on the door again.
"All done!", he says, and collects his money.  "By the way," the student says,
"That's not a Porsche, it's a Ferrari."
SEPARATOR
Look afar and see the end from the beginning.
SEPARATOR
"I'll rob that rich person and give it to some poor deserving slob.
 That will *prove* I'm Robin Hood."
-- Daffy Duck, Looney Tunes, _Robin Hood Daffy_
SEPARATOR
"I've seen, I SAY, I've seen better heads on a mug of beer"
		-- Senator Claghorn
SEPARATOR
	THE LESSER-KNOWN PROGRAMMING LANGUAGES #8: LAIDBACK

This language was developed at the Marin County Center for T'ai Chi,
Mellowness and Computer Programming (now defunct), as an alternative to
the more intense atmosphere in nearby Silicon Valley.

The center was ideal for programmers who liked to soak in hot tubs while
they worked.  Unfortunately few programmers could survive there because the
center outlawed Pizza and Coca-Cola in favor of Tofu and Perrier.

Many mourn the demise of LAIDBACK because of its reputation as a gentle and
non-threatening language since all error messages are in lower case.  For
example, LAIDBACK responded to syntax errors with the message:

	"i hate to bother you, but i just can't relate to that.  can
	you find the time to try it again?"
SEPARATOR
He has not acquired a fortune; the fortune has acquired him.
		-- Bion
SEPARATOR
Speaking as someone who has delved into the intricacies of PL/I, I am sure
that only Real Men could have written such a machine-hogging, cycle-grabbing,
all-encompassing monster.  Allocate an array and free the middle third?
Sure!  Why not?  Multiply a character string times a bit string and assign the
result to a float decimal?  Go ahead!  Free a controlled variable procedure
parameter and reallocate it before passing it back?  Overlay three different
types of variable on the same memory location?  Anything you say!  Write a
recursive macro?  Well, no, but Real Men use rescan.  How could a language
so obviously designed and written by Real Men not be intended for Real Man
use?
SEPARATOR
It is exactly because a man cannot do a thing that he is a proper judge of it.
		-- Oscar Wilde
SEPARATOR
	One day it was announced that the young monk Kyogen had reached
an enlightened state.  Much impressed by this news, several of his peers
went to speak with him.
	"We have heard that you are enlightened.  Is this true?" his fellow
students inquired.
	"It is", Kyogen answered.
	"Tell us", said a friend, "how do you feel?"
	"As miserable as ever", replied the enlightened Kyogen.
SEPARATOR
Lamonte Cranston once hired a new Chinese manservant.  While describing his
duties to the new man, Lamonte pointed to a bowl of candy on the coffee
table and warned him that he was not to take any.  Some days later, the new
manservant was cleaning up, with no one at home, and decided to sample some
of the candy.  Just than, Cranston walked in, spied the manservant at the
candy, and said:
	"Pardon me Choy, is that the Shadow's nugate you chew?"
SEPARATOR
Have you seen how Sonny's burning,
Like some bright erotic star,
He lights up the proceedings,
And raises the temperature.
		-- The Birthday Party, "Sonny's Burning"
SEPARATOR
May you do Good Magic with Perl.
             -- Larry Wall's blessing
SEPARATOR
brain, v: [as in "to brain"]
	To rebuke bluntly, but not pointedly; to dispel a source
	of error in an opponent.
		-- Ambrose Bierce, "The Devil's Dictionary"
SEPARATOR
This universe shipped by weight, not by volume.  Some expansion of the
contents may have occurred during shipment.
SEPARATOR
Langsam's Laws:
	(1) Everything depends.
	(2) Nothing is always.
	(3) Everything is sometimes.
SEPARATOR
The \`loner' may be respected, but he is always resented by his colleagues,
for he seems to be passing a critical judgment on them, when he may be
simply making a limiting statement about himself.
		-- Sidney Harris
SEPARATOR
"We don't care.  We don't have to.  We're the Phone Company."
SEPARATOR
Police up your spare rounds and frags.  Don't leave nothin' for the dinks.
- Willem Dafoe in "Platoon"
SEPARATOR
Did you hear that there's a group of South American Indians that worship
the number zero?

Is nothing sacred?
SEPARATOR
Oliver's Law:
	Experience is something you don't get until just after you need it.
SEPARATOR
<Palisade> how are we going to pronounce '00 or '01 or '02 and so on?
<Deek> Say goodbye to the nineties, say hello to the naughties. :)
SEPARATOR
QOTD:
	"My shampoo lasts longer than my relationships."
SEPARATOR
"To YOU I'm an atheist; to God, I'm the Loyal Opposition."
-- Woody Allen
SEPARATOR
Tussman's Law:
	Nothing is as inevitable as a mistake whose time has come.
SEPARATOR
I'm pretending I'm pulling in a TROUT!  Am I doing it correctly??
SEPARATOR
Jane and I got mixed up with a television show -- or as we call it back
east here: TV -- a clever contraction derived from the words Terrible
Vaudeville. However, it is our latest medium -- we call it a medium
because nothing's well done. It was discovered, I suppose you've heard,
by a man named Fulton Berle, and it has already revolutionized social
grace by cutting down parlour conversation to two sentences: "What's on
television?" and "Good night".
		-- Goodman Ace, letter to Groucho Marx, in The Groucho
		   Letters, 1967
SEPARATOR
A people living under the perpetual menace of war and invasion is very easy to
govern.  It demands no social reforms.  It does not haggle over expenditures
on armaments and military equipment.  It pays without discussion, it ruins
itself, and that is an excellent thing for the syndicates of financiers and
manufacturers for whom patriotic terrors are an abundant source of gain.
		-- Anatole France
SEPARATOR
We were so poor that we thought new clothes meant someone had died.
SEPARATOR
<Culus> "Hello?"  "Hi baybee"  "Are you Johnie Ingram?"  "For you I'll be
        anyone" "Ermm.. Do you sell slink CD's?" "I love slinkies"
SEPARATOR
It was a JOKE!!  Get it??  I was receiving messages from DAVID LETTERMAN!!
YOW!!
SEPARATOR
<Knghtbrd> I can think of lots of people who need USER=ID10T someplace!
SEPARATOR
How untasteful can you get?
SEPARATOR
Mid-Twenties Breakdown:
	A period of mental collapse occurring in one's twenties,
often caused by an inability to function outside of school or
structured environments coupled with a realization of one's essential
aloneness in the world.  Often marks induction into the ritual of
pharmaceutical usage.
		-- Douglas Coupland, "Generation X: Tales for an Accelerated
		   Culture"
SEPARATOR
No skis take rocks like rental skis!
SEPARATOR
I'd probably settle for a vampire if he were romantic enough.
Couldn't be any worse than some of the relationships I've had.
		-- Brenda Starr
SEPARATOR
Where will it all end?  Probably somewhere near where it all began.
SEPARATOR
If you wish to be happy for one hour, get drunk.
If you wish to be happy for three days, get married.
If you wish to be happy for a month, kill your pig and eat it.
If you wish to be happy forever, learn to fish.
		-- Chinese Proverb
SEPARATOR
His designs were strictly honourable, as the phrase is: that is, to rob
a lady of her fortune by way of marriage.
		-- Henry Fielding, "Tom Jones"
SEPARATOR
A national debt, if it is not excessive, will be to us a national blessing.
		-- Alexander Hamilton
SEPARATOR
I never vote for anyone.  I always vote against.
		-- W.C. Fields
SEPARATOR
God help the troubadour who tries to be a star.  The more that you try
to find success, the more that you will fail.
		-- Phil Ochs, on the Second System Effect
SEPARATOR
There's no justice in this world.
		-- Frank Costello, on the prosecution of "Lucky" Luciano by
		   New York district attorney Thomas Dewey after Luciano had
		   saved Dewey from assassination by Dutch Schultz (by ordering
		   the assassination of Schultz instead)
SEPARATOR
Real software engineers don't debug programs, they verify correctness.
This process doesn't necessarily involve execution of anything on a
computer, except perhaps a Correctness Verification Aid package.
SEPARATOR
Magically turning people's old scalar contexts into list contexts is a
recipe for several kinds of disaster.
             -- Larry Wall in <199709291631.JAA08648@wall.org>
SEPARATOR
Don't get even -- get odd!
SEPARATOR
I was playing poker the other night... with Tarot cards. I got a full house and
4 people died.
-- Steven Wright
SEPARATOR
Fresco's Discovery:
	If you knew what you were doing you'd probably be bored.
SEPARATOR
It's appositival, if it's there.  And it doesn't have to be there.
And it's really obvious that it's there when it's there.
             -- Larry Wall in <199709032332.QAA21669@wall.org>
SEPARATOR
"I wonder if this is the first constitution in the history of mankind
where you have to calculate a square root to determine if a motion
passes.  :-)"
        -- Seen on Slashdot
SEPARATOR
Live fast, die young, and leave a flat patch of fur on the highway!
		-- The Squirrels' Motto (The "Hell's Angels of Nature")
SEPARATOR
This was the most unkindest cut of all.
		-- William Shakespeare, "Julius Caesar"
SEPARATOR
There is no grief which time does not lessen and soften.
SEPARATOR
Since this database is not used for profit, and since entire works are not
published, it falls under fair use, as we understand it.  However, if any
half-assed idiot decides to make a profit off of this, they will need to
double check it all...
        -- Notes included with the default fortunes database
SEPARATOR
If you try to please everyone, somebody is not going to like it.
SEPARATOR
Excuse me, but didn't I tell you there's NO HOPE for the survival of
OFFSET PRINTING?
SEPARATOR
The people rule.
SEPARATOR
The bugs you have to avoid are the ones that give the user not only
the inclination to get on a plane, but also the time.
		-- Kay Bostic
SEPARATOR
When all else fails, try Kate Smith.
SEPARATOR
Women waste men's lives and think they have indemnified them by a few
gracious words.
		-- Honor'e de Balzac
SEPARATOR
A man's best friend is his dogma.
SEPARATOR
Support Bingo, keep Grandma off the streets.
SEPARATOR
Q:	What's hard going in and soft and sticky coming out?
A:	Chewing gum.
`;

const fortunes = raw.split('SEPARATOR\n');
export default fortunes;
