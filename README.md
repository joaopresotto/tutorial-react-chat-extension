alterações app-chat:

- envio de mensagem com ctrl+enter;
- não permite envio de mensagem vazia;
- mudança no visual;
- mostra tempo das mensagens;


Para testar a aplicação:

- clone ou baixe o repositório;
- navegue até a pasta app do repositório em um terminal;
- digite o comando 'npm install';
- digite o comando 'npm start' e o chat vai abrir;